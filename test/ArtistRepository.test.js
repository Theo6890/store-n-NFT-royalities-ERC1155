// Created contracts
const DAI = artifacts.require("DAI");
const ForceSend = artifacts.require("ForceSend");
const Marketplace = artifacts.require("Marketplace");
const ArtistRepository = artifacts.require("ArtistRepository");
//Module loading
require("chai").use(require("chai-as-promised")).should();
const { expect } = require("chai");
const truffleAssert = require("truffle-assertions");
const Utils = require("./Utils.js");

contract("ArtistRepository & Marketplace", (accounts) => {
  let store, artistRepo, dai;
  const bandName = "AC/DC band mocked",
    songName1 = "Thunderstruck",
    songName2 = "T.N.T",
    hash1 = "Qm234Scsft5765dfrRGERfPloiN879",
    hash2 = "QmEZGQqerbsthndt890345VtvZ35",
    binance8 = "0xF977814e90dA44bFA03b6295A0616a897441aceC";

  before(async () => {
    store = await Marketplace.new();
    // Artist deploy their repo
    artistRepo = await ArtistRepository.new(store.address, bandName, 500000);
    dai = await DAI.at("0x6b175474e89094c44da98b954eedeac495271d0f");
  });

  describe("Artist fill their repository", async () => {
    it("verifies royality amount", async () => {
      let res = await artistRepo.royaltyInfo();

      res = parseFloat(res[1] / 100000);
      expect(res).to.be.eq(5);
    });

    it("adds 2 songs", async () => {
      await artistRepo.addWork(songName1, hash1);
      const res = await artistRepo.addWork(songName2, hash2);

      truffleAssert.eventEmitted(res, "WorkAdded", (result) => {
        const id = parseFloat(result.id);
        return expect(id).to.be.equal(2);
      });
    });

    it("gets all added songs", async () => {
      const result = await artistRepo.getWorks();

      expect(result.name).to.be.equal(bandName);
      expect(result.songs[0]).to.be.equal(songName1);
      expect(result.songs[1]).to.be.equal(songName2);
    });

    it("mints 5 copies of one song to a selected address", async () => {
      const mint = await artistRepo.mint(
        accounts[1],
        2,
        5,
        web3.utils.asciiToHex(hash1) // string to bytes
      );

      truffleAssert.eventEmitted(mint, "TransferSingle", (result) => {
        const id = parseFloat(result.id);
        const value = parseFloat(result.value);
        return (
          expect(result.to).to.be.equal(accounts[1]) &&
          expect(id).to.be.equal(2) &&
          expect(value).to.be.equal(5)
        );
      });
    });

    it("Sucesses: the selected address getHashById(id)", async () => {
      const hash = await artistRepo.getHashById(2, {
        from: accounts[1],
      });

      expect(hash).to.be.eq(hash2);
    });

    it("Sucesses: the selected address getHashByName(songName)", async () => {
      const hash = await artistRepo.getHashByName(songName2, {
        from: accounts[1],
      });

      expect(hash).to.be.eq(hash2);
    });

    it("Fails: someone random tries to access the hash", async () => {
      await truffleAssert.fails(
        artistRepo.getHashById(1, {
          from: accounts[3],
        }),
        "Work not owned"
      );
    });
  });

  describe("Marketplace actions", async () => {
    const workAmount = 2;
    let buyRes, singleSongPrice;

    before(async () => {
      const forceSend = await ForceSend.new();
      await forceSend.go(store.address, { value: Utils.toWei("10") });

      await dai.transfer(accounts[4], Utils.toWei("1000"), {
        from: binance8,
      });

      singleSongPrice = await store.singleSongPrice();

      await dai.approve(store, workAmount * parseFloat(singleSongPrice), {
        from: accounts[4],
      });

      buyRes = await store.buyWork(artistRepo.address, songName1, workAmount, {
        from: accounts[4],
      });
    });

    it("verifies price of a single song", async () => {
      singleSongPrice = parseFloat(Utils.fromWei(singleSongPrice));
      expect(singleSongPrice).to.be.eq(0.01);
    });

    it("verifies Marketplace received payment from buyer", async () => {
      let balance = await dai.balanceOf(store.address);
      balance = parseFloat(Utils.toWei(balance));
      expect(balance).to.be.eq(20);
    });

    it.skip("verifies buyer received the token", async () => {
      let balance = await artistRepo.balanceOf(accounts[4], 1);
      balance = parseFloat(balance);
      expect(balance).to.be.eq(2);
    });

    it.skip("verifies artist receive royalities", async () => {
      truffleAssert.eventEmitted(buyRes, "ReceivedRoyalties", (result) => {
        const recipient = result[0];
        const buyer = result[1];
        const tokenId = result[2];
        const tokenPaid = result[3];
        const amountRoyalities = parseFloat(Utils.fromWei(result[4]));
        return (
          expect(recipient).to.be.equal(artistRepo.artist()) &&
          expect(buyer).to.be.equal(accounts[4]) &&
          expect(tokenId).to.be.equal(1) &&
          expect(tokenPaid).to.be.equal(0x0) &&
          expect(amountRoyalities).to.be.equal(0.02)
        );
      });
    });
  });
});
