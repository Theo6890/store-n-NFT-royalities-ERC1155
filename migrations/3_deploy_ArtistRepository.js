const Marketplace = artifacts.require("Marketplace");
const ArtistRepository = artifacts.require("ArtistRepository");

module.exports = async function (deployer, network, accounts) {
  Marketplace.deployed().then(async (store) => {
    return deployer.deploy(
      ArtistRepository,
      store.address,
      "AC/DC band mocked",
      500000
    );
  });
};
