// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./ERC2981.sol";

import "./Marketplace.sol";

/* 
    @notice: Due to ERC-1155 (multi tokens standard) we can imagine a child contract inhereting from it being 
             an artit's repository. All their work will be stored in here.
             A work can be produced as a single unique piece (e.g. a painting) or it can have a limited 
             supply (e.g. a collector album) or it can have an unlimited supply (e.g. a song).
             Supply will manage in here and only marketplace or artist can mint new copy of slected work
             according on the max supply definied when a work is added.  
             Royalities standard (EIP-2981 atm) is required so a marketplace can pays royalities to the 
             artist.
             Work's price, buy and sell are marketplace stuff so nothing like that is implemented in here.

    @link: https://github.com/ethereum/EIPs/issues/2907#issuecomment-800678180 
*/
contract ArtistRepository is ERC1155, ERC2981 {
    string public artistName;
    string[] private _works;

    mapping(string => uint256) private _idOf;
    mapping(uint256 => string) private _hashOf;

    address public artist;
    address public store; // there can be multiple stores
    // address[] public acceptedCurrencies; // Artists can choose which currencies they accept

    using Counters for Counters.Counter;
    Counters.Counter private _workIds;

    event WorkAdded(uint256 id, string workName, string ipfsHash);
    event ArtistNameChanged(string name);
    event ArtistAddrUpdated(address addr);
    event StoreUpdated(address store);

    modifier onlyArtist() {
        require(_msgSender() == artist, "Only artist can do so");
        _;
    }

    modifier onlyStoreOrArtist() {
        require(
            _msgSender() == artist || _msgSender() == store,
            "Only artist or store can do so"
        );
        _;
    }

    modifier onlyWorkOwnersOrArtist(uint256 id) {
        require(
            (balanceOf(_msgSender(), id) != 0) || _msgSender() == artist,
            "Work not owned"
        );
        _;
    }

    constructor(
        Marketplace store_,
        string memory artistName_,
        uint256 royalityAmount_
    ) ERC1155("https://ipfs.io/ipfs/") ERC2981(royalityAmount_, _msgSender()) {
        artist = _msgSender();
        artistName = artistName_;
        store = address(store_);
    }

    function getWorks()
        external
        view
        returns (string memory name, string[] memory songs)
    {
        return (artistName, _works);
    }

    function getIdOf(string memory workName) external view returns (uint256) {
        return _idOf[workName];
    }

    function getHashById(uint256 id)
        external
        view
        onlyWorkOwnersOrArtist(id)
        returns (string memory)
    {
        return _hashOf[id];
    }

    function getHashByName(string memory workName)
        external
        view
        onlyWorkOwnersOrArtist(_idOf[workName])
        returns (string memory)
    {
        uint256 id = _idOf[workName];
        return _hashOf[id];
    }

    function mint(
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data
    ) public onlyStoreOrArtist {
        ERC1155._mint(to, id, amount, data);
    }

    function mintBatch(
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) public onlyStoreOrArtist {
        ERC1155._mintBatch(to, ids, amounts, data);
    }

    function updateArtistName(string memory artistName_) public onlyArtist {
        artistName = artistName_;
        emit ArtistNameChanged(artistName);
    }

    function updateArtistAddr(address addr) public onlyArtist {
        artist = addr;
        emit ArtistAddrUpdated(artist);
    }

    function updateStoreAddr(address store_) public onlyArtist {
        store = store_;
        emit StoreUpdated(store);
    }

    // @dev: Adding a new work means: incrementing _workIds & saving work's hash
    function addWork(string memory workName, string memory ipfsHash)
        public
        onlyArtist
    {
        _workIds.increment();
        _works.push(workName);
        _idOf[workName] = _workIds.current();
        _hashOf[_workIds.current()] = ipfsHash;

        emit WorkAdded(_workIds.current(), workName, ipfsHash);
    }
}
