// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./IERC2981.sol";

abstract contract ERC2981 is IERC2981 {
    uint256 private royaltyAmount;
    address private creator;

    constructor(uint256 _amount, address _creator) {
        royaltyAmount = _amount;
        creator = _creator;
    }

    function royaltyInfo()
        external
        view
        override
        returns (address receiver, uint256 amount)
    {
        return (creator, royaltyAmount);
    }

    function receivedRoyalties(
        address _royaltyRecipient,
        address _buyer,
        uint256 _tokenId,
        address _tokenPaid,
        uint256 _amount
    ) external override {
        emit ReceivedRoyalties(
            _royaltyRecipient,
            _buyer,
            _tokenId,
            _tokenPaid,
            _amount
        );
    }
}
