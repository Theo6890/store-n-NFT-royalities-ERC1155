// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./ArtistRepository.sol";
import "./IERC2981.sol";

/*
    @notice: Marketplace does marketplace stuff: buy, sell, set price of a work (artistRepo set it up) 
             & pays royalities.
    https://github.com/ethereum/EIPs/issues/2907#issuecomment-800678180
*/
contract Marketplace is Context {
    uint256 public singleSongPrice = 1 * 10**19; // 10 $DAI in Wei

    function buyWork(
        ArtistRepository artistRepo,
        string memory workName,
        uint256 amount_
    ) public payable {
        uint256 songId = artistRepo.getIdOf(workName);
        uint256 amount = amount_ > 1 ? amount_ : 1;

        // User pays the work's price
        IERC20(0x6B175474E89094C44Da98b954EedeAC495271d0F).transfer(
            address(this),
            singleSongPrice * amount
        );

        // Marketplace create a new work from selected artist
        artistRepo.mint(msg.sender, songId, amount, "");

        // //It pays royalities
        // (address royalitiesReceiver, uint256 percentage) =
        //     artistRepo.royaltyInfo();
        // uint256 rAmount = (singleSongPrice * amount * percentage) / 100000;
        // payable(royalitiesReceiver).transfer(rAmount);
        // IERC2981(artistRepo).receivedRoyalties(
        //     royalitiesReceiver,
        //     _msgSender(),
        //     songId,
        //     address(0x6B175474E89094C44Da98b954EedeAC495271d0F),
        //     amount
        // );
    }
}
